# voxl-mongoose

This is a build of the [mongoose](https://github.com/cesanta/mongoose) web server library for voxl


## Build Instructions

1) prerequisite: the voxl-cross docker image >= V1.7

Follow the instructions here to build and install the voxl-cross docker image:

https://gitlab.com/voxl-public/voxl-docker

2) Launch the voxl-cross docker and make sure this project directory is mounted inside the Docker.

```bash
~/git/voxl-mongoose$ voxl-docker -i voxl-cross
voxl-cross:~/voxl-mongoose$
voxl-cross:~/voxl-mongoose$ ls
CHANGELOG       LICENSE    build.sh  conf     install_on_voxl.sh  src             readers
CMakeLists.txt  README.md  clean.sh  include  ipk                 make_package.sh
voxl-cross:~/voxl-mongoose$
```

3) Build for either `qrb5165` or `apq8096`. CI will pass these arguments to the build script based on the job target. The build script in this template will build a multi-arch package with binaries for both 64 and 32 bit.


```bash
voxl-cross:~$ ./build.sh qrb5165
OR
voxl-cross:~$ ./build.sh apq8096
```

5) Make an ipk or deb package while still inside the docker.

```bash
voxl-cross:~$ ./make_package.sh deb
OR
voxl-cross:~$ ./make_package.sh ipk
```

This will make a new package file in your working directory. The name and version number came from the package control file. If you are updating the package version, edit it there.

Optionally add the --timestamp argument to append the current data and time to the package version number in the debian package. This is done automatically by the CI package builder for development and nightly builds, however you can use it yourself if you like.

## Deploy to VOXL

You can now push the ipk or deb package to VOXL and install with dpkg/opkg however you like. To do this over ADB, you may use the included helper script: deploy_to_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

The deploy_to_voxl.sh script will query VOXL over adb to see if it has dpkg installed. If it does then then .deb package will be pushed an installed. Otherwise the .ipk package will be installed with opkg.

```bash
(outside of docker)
voxl-mongoose$ ./deploy_to_voxl.sh
```

This deploy script can also push over a network given sshpass is installed and the VOXL uses the default root password.

```bash
(outside of docker)
voxl-mongoose$ ./deploy_to_voxl.sh ssh 192.168.1.123
```


